<?php
  if (isset($_POST['project'])) {
    $files = glob("*");
    $msg;
    foreach ($files as $key => $value) {
      if (is_dir($value)) {
        if ($value == $_POST['project']) {
          $exists = true;
        }
      }
    }

    $status = "red";
    $msg = "The project `".$_POST['project']."` already exists!";

    if (!$exists) {
      $msg = "There was a problem creating the project `".$_POST['project']."`";
      if (mkdir($_POST['project']) && mkdir($_POST['project']."/res") && mkdir($_POST['project']."/res/css") && mkdir($_POST['project']."/res/js") && mkdir($_POST['project']."/res/images")) {
        $content = '
          <!DOCTYPE html>
          <html lang="en" dir="ltr">
            <head>
              <meta charset="utf-8">
              <title>'.$_POST['project'].' | Home</title>
              <link rel="stylesheet" href="res/css/main.css">
            </head>
            <body>
              <h1>'.$_POST['project'].' Project</h1>
              <p>You are viewing the default project homepage</p>
              <br>
              <p>You can edit the project <a href="#">here</a></p>
              <script src="res/js/main.js"></script>
            </body>
          </html>
        ';

        $index = fopen($_POST['project']."/index.php", "w");
        fwrite($index, $content);

        $content = "
          html, body {
            margin: 0;
            width: 100%;
            height: 100%;
            text-align: center;
          }

          body {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
          }

          h1 {
            font-size: 72px;
            font-family: 'Nunito', sans-serif;
            margin: 0.2em 0;
            color: #808080;
          }

          p {
            font-size: 17px;
            font-family: 'Ubuntu Condensed', sans-serif;
          }
        ";

        $css = fopen($_POST['project']."/res/css/main.css", "w");
        fwrite($css, $content);

        $js = fopen($_POST['project']."/res/js/main.js", "w");
        fwrite($js, "");
        $msg = "The project `<a href='atom://open?url=file:///".getcwd()."/".$_POST['project']."/index.php'>".$_POST['project']."</a>` was created successfully!";
        $status = "green";
      }
    }
  }
?>


<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Kaya PHP Workspace</title>
    <style>
      html, body {
        margin: 0;
        width: 100%;
        height: 100%;
        text-align: center;
      }

      body {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
      }

      h1 {
        font-size: 72px;
        font-family: 'Nunito', sans-serif;
        margin: 0.2em 0;
        color: #808080;
      }

      h2 {
        font-size: 30px;
        font-family: 'Ubuntu Condensed', sans-serif;
        color: #404040;
      }

      div.msg {
        padding: 0.5em 1em;
        font-size: 17px;
        font-family: 'Nunito', sans-serif;
        display: flex;
        align-items: center;
        justify-content: center;
        color: #fff;
      }

      div.green {
        background-color: #00cc66;
      }

      div.red {
        background-color: #ff4d4d;
      }

      div.msg button {
        font-size: 30px;
        font-family: 'Nunito', sans-serif;
        cursor: pointer;
        margin-left: 0.5em;
        color: #fff;
        background: none;
        border: none;
      }

      div.create {
        padding: 2em;
      }

      input[type="text"] {
        padding: 1em;
        font-size: 17px;
        font-family: "Nunito", sans-serif;
        color: #808080;
        border: none;
        border-bottom: 1px solid #404040;
        text-align: center;
      }

      div.projects {
        display: grid;
        grid-template-columns: repeat(9, 1fr);
        grid-gap: 20px;
      }

      a.projects {
        text-decoration: none;
        font-family: 'Nunito', sans-serif;
        font-size: 16px;
        color: #404040;
        border: 1px solid #808080;
        border-radius: 5px;
        transition: 0.3s;
        transition-delay: 0.1s;
        padding: 0.5em 1em;
      }

      a {
        color: #fff;
      }

      a.projects:hover {
        background-color: #808080;
        color: #fff;
      }
    </style>
  </head>
  <body>
    <?php
      if (isset($_POST['project'])) {
        echo '
          <div class="msg '.$status.'">
            <p>'.$msg.'</p>
            <button>X</button>
          </div>
        ';
      }
    ?>
    <h1>Kaya PHP Workspace</h1>
    <div class="create">
      <form action="/" method="post">
        <input placeholder="Create New Project" type="text" name="project" value="">
      </form>
    </div>
    <div class="container">
      <h2>Projects</h2>
      <div class="projects">
        <?php
          $files = glob("*");
          foreach ($files as $key => $value) {
            if (is_dir($value)) {
              echo "<a href='".$value."' class='projects'>".$value."</a>";
            }
          }
        ?>
      </div>
    </div>
  </body>
</html>
